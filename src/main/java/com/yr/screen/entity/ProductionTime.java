package com.yr.screen.entity;

import lombok.Data;

/**
 * @author xzl
 * @date 2023-10-12 16:36
 **/
@Data
public class ProductionTime {
    private String totalTime;
    private String dayAvgProductionTime;
    private String dayAvgIdleTime;
}
