package com.yr.screen.entity;

import lombok.Data;

/**
 * @author xzl
 * @date 2023-10-12 16:14
 **/
@Data
public class DeviceStatistics {
    private String runNum;
    private String faultNum;
    private String standbyNum;
    private String offlineNum;
}
