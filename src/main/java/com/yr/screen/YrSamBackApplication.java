package com.yr.screen;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xuzilou
 */
@MapperScan({"com.yr.screen.mapper"})
@SpringBootApplication
public class YrSamBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(YrSamBackApplication.class, args);
    }

}
