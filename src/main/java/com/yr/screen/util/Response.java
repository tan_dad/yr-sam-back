package com.yr.screen.util;

import cn.hutool.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * 公共响应
 *
 * @author xuzilou
 * @date 2021/4/6 9:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> implements Serializable {

    private T data;

    private Integer code;

    private String msg;

    /**
     * 响应成功
     *
     * @param data 响应数据
     * @param <T>  泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> ok(T data) {
        return ok(data, "响应成功");
    }

    /**
     * 响应成功
     *
     * @param <T> 泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> ok() {
        return ok(null, "响应成功");
    }

    /**
     * 响应成功
     *
     * @param data 响应数据
     * @param msg  响应消息
     * @param <T>  泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> ok(T data, String msg) {
        return new Response<>(data, HttpStatus.HTTP_OK, msg);
    }

    /**
     * 响应失败
     *
     * @param <T> 泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> error() {
        return error(HttpStatus.HTTP_INTERNAL_ERROR, "响应失败");
    }

    /**
     * 响应失败
     *
     * @param msg 失败消息
     * @param <T> 泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> error(String msg) {
        return error(HttpStatus.HTTP_INTERNAL_ERROR, msg);
    }


    /**
     * 响应失败
     *
     * @param code 状态吗
     * @param msg  失败消息
     * @param <T>  泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> error(Integer code, String msg) {
        return new Response<>(null, code, msg);
    }

    /**
     * 响应失败
     *
     * @param data 响应数据
     * @param code 状态码
     * @param msg  失败消息
     * @param <T>  泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> error(T data, Integer code, String msg) {
        return new Response<>(data, code, msg);
    }

    /**
     * 包装
     *
     * @param flag 包装响应
     * @param <T>  泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> wrap(boolean flag) {
        return flag ? ok() : error();
    }

    /**
     * 包装
     *
     * @param msg  响应消息
     * @param flag 包装响应
     * @param <T>  泛型参数
     * @return {@link Response}
     */
    public static <T> Response<T> wrap(boolean flag, String msg) {
        return flag ? ok(null, msg) : error(msg);
    }


    /**
     * 是否响应成功
     *
     * @return {@link Boolean}
     */
    public boolean isSuccess() {
        return Objects.equals(this.code, HttpStatus.HTTP_OK);
    }
}
