package com.yr.screen.controller;

import com.yr.screen.entity.DeviceStatistics;
import com.yr.screen.entity.ProductionTime;
import com.yr.screen.service.BigScreenService;
import com.yr.screen.util.Response;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author xzl
 * @date 2023-10-12 14:28
 **/
@CrossOrigin(origins = "*")
@RestController
public class BigScreenController {

    @Resource
    private BigScreenService bigScreenService;

    @GetMapping("/test")
    private Response<String> test() {
        return Response.ok("hello");
    }

    @PostMapping("/deviceStatistics")
    private Response<DeviceStatistics> deviceStatistics() {
        return Response.ok(bigScreenService.deviceStatistics());
    }

    @PostMapping("/productionTime")
    private Response<ProductionTime> productionTime() {
        return Response.ok(bigScreenService.productionTime());
    }

    @PostMapping("/deviceEfficiency")
    private Response<String> deviceEfficiency() {
        return Response.ok(bigScreenService.deviceEfficiency());
    }
}
