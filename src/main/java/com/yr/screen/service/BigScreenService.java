package com.yr.screen.service;

import com.yr.screen.entity.DeviceStatistics;
import com.yr.screen.entity.ProductionTime;

/**
 * @author xzl
 * @date 2023-10-12 14:30
 **/
public interface BigScreenService {
    DeviceStatistics deviceStatistics();

    ProductionTime productionTime();

    String deviceEfficiency();

}
