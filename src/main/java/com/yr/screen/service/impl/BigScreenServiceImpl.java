package com.yr.screen.service.impl;

import com.yr.screen.entity.DeviceStatistics;
import com.yr.screen.entity.ProductionTime;
import com.yr.screen.mapper.BigScreenMapper;
import com.yr.screen.service.BigScreenService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author xzl
 * @date 2023-10-12 14:30
 **/
@Service
public class BigScreenServiceImpl implements BigScreenService {
    @Resource
    private BigScreenMapper bigScreenMapper;


    @Override
    public DeviceStatistics deviceStatistics() {
        DeviceStatistics deviceStatistics = new DeviceStatistics();
        deviceStatistics.setFaultNum("21");
        deviceStatistics.setRunNum("22");
        deviceStatistics.setStandbyNum("23");
        deviceStatistics.setOfflineNum("24");
        return deviceStatistics;
    }

    @Override
    public ProductionTime productionTime() {
        ProductionTime productionTime = new ProductionTime();
        productionTime.setDayAvgProductionTime("456");
        productionTime.setTotalTime("321");
        productionTime.setDayAvgIdleTime("123");
        return productionTime;
    }

    @Override
    public String deviceEfficiency() {
        return "30";
    }
}
